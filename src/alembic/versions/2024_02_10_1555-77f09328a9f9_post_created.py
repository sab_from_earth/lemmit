"""Post created

Revision ID: 77f09328a9f9
Revises: 2227925fa8ff
Create Date: 2024-02-10 15:55:00.187379

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import table, column, func

# revision identifiers, used by Alembic.
revision = '77f09328a9f9'
down_revision = '2227925fa8ff'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.add_column(sa.Column('created', sa.DateTime(), nullable=True))

    post_table = table('posts',
                       column('id', sa.Integer),
                       column('updated', sa.DateTime),
                       column('created', sa.DateTime))

    op.execute(
        post_table.update().values(created=post_table.c.updated)
    )

    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.alter_column('created', existing_type=sa.DateTime(), nullable=False, default=func.now())

    op.create_index('ix_posts_created', 'posts', ['created'], unique=False)


def downgrade():
    with op.batch_alter_table('posts', schema=None) as batch_op:
        batch_op.drop_index('ix_posts_created')
        batch_op.drop_column('created')

