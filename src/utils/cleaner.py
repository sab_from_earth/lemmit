import logging
from datetime import datetime, timedelta

from sqlalchemy import text, func
from sqlalchemy.orm import Session as DbSession

from models.models import Post, Community

# Configure logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

KEEP_PER_COMMUNITY = 500
KEEP_MIN_DAYS = 30


class Cleaner:
    def __init__(self, db: DbSession):
        self._db: DbSession = db

    def trim_posts(self, read_only: bool = True) -> int:
        # Calculate the minimum allowed date
        min_allowed_date = datetime.utcnow() - timedelta(days=KEEP_MIN_DAYS)

        # Select communities with more than KEEP_PER_COMMUNITY posts and posts older than KEEP_MIN_DAYS
        eligible_communities = self._db.query(
            Post.community_id, Community.ident
        ).join(
            Community, Post.community_id == Community.id
        ).filter(
            Post.created < min_allowed_date
        ).group_by(
            Post.community_id
        ).having(
            func.count(Post.id) > KEEP_PER_COMMUNITY
        ).all()

        total_deleted = 0
        for community_id, community_ident in eligible_communities:
            # Find pivot date for each eligible community
            pivot_post = self._db.query(Post.created)\
                .filter(Post.community_id == community_id)\
                .order_by(Post.created.desc())\
                .offset(KEEP_PER_COMMUNITY - 1)\
                .first()

            if pivot_post:
                pivot_date = pivot_post.created
            else:
                # This case might actually not occur due to the pre-filtering
                continue

            # Determine the final cutoff date considering both KEEP_MIN_DAYS and pivot_date
            final_cutoff_date = min(pivot_date, min_allowed_date)

            if not read_only:
                # Delete posts older than final_cutoff_date for the community
                deleted_count = self._db.query(Post)\
                    .filter(Post.community_id == community_id,
                            Post.created < final_cutoff_date)\
                    .delete(synchronize_session=False)
                self._db.commit()
                total_deleted += deleted_count
            else:
                # Simulate deletion for read_only mode
                deleted_count = self._db.query(Post.id)\
                    .filter(Post.community_id == community_id,
                            Post.created < final_cutoff_date)\
                    .count()
                total_deleted += deleted_count

            logger.info(f"Community {community_ident} (ID {community_id}): Deleted {deleted_count} posts.")

        return total_deleted

    def vacuum_database(self):
        try:
            logger.info(f"Vacuuming...")
            self._db.commit()
            self._db.execute(text('VACUUM'))
            self._db.commit()  # Commit again to finalize the VACUUM operation
            logger.info("Database VACUUM completed successfully.")
        except Exception as e:
            logger.error(f"Failed to VACUUM database: {e}")
