import unittest
from datetime import datetime, timezone
from unittest import mock
from unittest.mock import MagicMock

import requests_mock

from models.models import CommunityDTO, PostDTO
from reddit.reader import RedditReader
from tests import get_test_data, TEST_POSTS, utc_now


class RedditReaderTestCase(unittest.TestCase):
    def setUp(self):
        self.subject = RedditReader()

        # Mock the logger
        self.subject.logger = mock.Mock()

    def tearDown(self):
        pass

    def test_get_subreddit_info(self):
        body = get_test_data('today_i_learned.html')
        self.subject._request = mock.Mock()
        self.subject.is_sub_nsfw = mock.Mock()

        self.subject._request.return_value = MagicMock(status_code=200, text=body)
        self.subject.is_sub_nsfw.return_value = False

        community = self.subject.get_subreddit_info('todayilearned')

        self.assertEqual(community, CommunityDTO(
            ident='todayilearned',
            title="Today I Learned (TIL)",
            description='You learn something new every day; what did you '
                        'learn today? Submit interesting and specific facts '
                        'about something that you just found out here.',
            icon="https://b.thumbs.redditmedia.com/pskDeiR7LPmkU3Vq1HSBs6Y0geRbSTAQiz23AwVppbs.jpg",
            nsfw=False
        ))
        self.subject.is_sub_nsfw.assert_called_once_with('todayilearned')
        self.subject._request.assert_called_once_with('GET', 'https://old.reddit.com/r/todayilearned/')

    def test_is_sub_nsfw(self):
        with requests_mock.Mocker() as m:
            m.get('https://old.reddit.com/r/gonewildaudio/',
                  status_code=302,
                  headers={'Location': 'https://old.reddit.com/over18?dest=https%3A%2F%2Fold.reddit.com%2Fr%2Fgonewildaudio%2F'})
            m.get('https://old.reddit.com/over18?dest=https%3A%2F%2Fold.reddit.com%2Fr%2Fgonewildaudio%2F',
                  text='Are you sure you\'re over 18?',
                  status_code=200)
            result = self.subject.is_sub_nsfw('gonewildaudio')
        self.assertTrue(result)

    def test_get_subreddit_ident(self):
        tests = [
            ['https://www.reddit.com/r/explainlikelimfive', 'explainlikelimfive'],
            ['/r/modsupport', 'modsupport'],
            ['r/bestof', 'bestof'],
            ['http://old.reddit.com/r/redditalternatives', 'redditalternatives'],
        ]

        for link, expected in tests:
            try:
                ident = RedditReader.get_subreddit_ident(link)
            except ValueError as e:
                ident = str(e)
            self.assertEqual(expected, ident)

    def test_get_subreddit_topics_json(self):
        body = get_test_data('subreddit.json')

        with requests_mock.Mocker() as m:
            m.get('https://old.reddit.com/r/programmerhumor/.json', text=body)
            result = self.subject.get_subreddit_topics_json('programmerhumor')

        self.assertEqual(len(result), 25)
        self.assertEqual(result[0], PostDTO(
            reddit_link='https://old.reddit.com/r/ProgrammerHumor/comments/1cnfy8i/ifyoudontliftyoudontcode/',
            title='thisIsActuallyMockedData',
            created=datetime(2024, 5, 8, 21, 29, 12, tzinfo=timezone.utc),
            updated=datetime(2024, 5, 8, 21, 29, 12, tzinfo=timezone.utc),
            author='/u/butWeWereOnBreak',
            external_link='https://i.redd.it/k8qih3adt9zc1.jpeg',
            body=None,
            nsfw=False,
            upvotes=5130,
            upvote_ratio=0.97
        ))

    def test_get_post_details_external(self):
        body = get_test_data('today_i_learned_post.html')
        with requests_mock.Mocker() as m:
            m.get('https://old.reddit.com/r/foobar/1', text=body)
            result = self.subject.get_post_details(TEST_POSTS[0])
        self.assertEqual(result, PostDTO(
            reddit_link='https://www.reddit.com/r/foobar/1',
            title='post 1',
            created=utc_now,
            updated=utc_now,
            author='/u/user1',
            external_link='https://en.wikipedia.org/wiki/Antilia_(building)',
            body=None,
            nsfw=False,
            upvotes=55,
            upvote_ratio=0.8,
        ))

    def test_get_post_details_self(self):
        body = get_test_data('funny_post.html')
        with requests_mock.Mocker() as m:
            m.get('https://old.reddit.com/r/foobar/1', text=body)
            result = self.subject.get_post_details(TEST_POSTS[0])
        self.assertEqual(result, PostDTO(
            reddit_link='https://www.reddit.com/r/foobar/1',
            title='post 1',
            created=utc_now,
            updated=utc_now,
            author='/u/user1',
            external_link=None,
            body="""Mr McGraw, a great teacher, very short, a national water skiing champ, SAID: "This is not a ruler!" while holding up a measuring device used to scale drawings, "I am the only ruler in this class. This is a scale!" I've been around for 75 years and when I think of it, it always elicits a chuckle.""",
            nsfw=False,
            upvotes=55,
            upvote_ratio=0.8,
        ))

